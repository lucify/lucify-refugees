# The flow towards Europe

This project moved to [GitHub](https://github.com/lucified/lucify-refugees). You can update your git remote with:

```
git remote set-url origin git@github.com:lucified/lucify-refugees.git
```
